from random import random

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision as tv
from torchinfo import summary


class LeNet(nn.Module):
    """LeNet (32x32 input)."""
    def __init__(self, input_channels=3, output_classes=10):
        super().__init__()
        self.convolutions = nn.Sequential(
            nn.Conv2d(input_channels, 6, kernel_size=(5, 5)),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(6, 16, kernel_size=(5, 5)),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(16, 120, kernel_size=(5, 5)),
            nn.ReLU(),
        )
        self.linear = nn.Sequential(
            nn.Linear(120, 84),
            nn.ReLU(),
            nn.Linear(84, output_classes),
        )

    def forward(self, x: torch.Tensor):
        x = F.interpolate(x, size=(32, 32), mode='nearest')  # nearest | linear | bilinear | bicubic | trilinear

        x = self.convolutions(x)
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x


class LeNet_BN(nn.Module):
    """LeNet (32x32 input) with Batch Normalization."""
    def __init__(self, input_channels=3, output_classes=10):
        super().__init__()
        self.convolutions = nn.Sequential(
            nn.Conv2d(input_channels, 6, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(6),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(6, 16, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(16, 120, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(120),
            nn.ReLU(),
        )
        self.linear = nn.Sequential(
            nn.Linear(120, 84, bias=False),
            nn.BatchNorm1d(84),
            nn.ReLU(),
            nn.Linear(84, output_classes),
        )

    def forward(self, x: torch.Tensor):
        x = F.interpolate(x, size=(32, 32), mode='nearest')  # nearest | linear | bilinear | bicubic | trilinear

        x = self.convolutions(x)
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x


class LeNet_DO(nn.Module):
    def __init__(self, input_channels=3, output_classes=10, do=0):
        super().__init__()
        self.convolutions = nn.Sequential(
            nn.Conv2d(input_channels, 6, kernel_size=(5, 5)),
            nn.ReLU(),
            nn.Dropout2d(do),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(6, 16, kernel_size=(5, 5)),
            nn.ReLU(),
            nn.Dropout2d(do),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(16, 120, kernel_size=(5, 5)),
            nn.ReLU(),
            nn.Dropout2d(do),
        )
        self.linear = nn.Sequential(
            nn.Linear(120, 84),
            nn.ReLU(),
            nn.Dropout(do),
            nn.Linear(84, output_classes),
        )

    def forward(self, x: torch.Tensor):
        x = F.interpolate(x, size=(32, 32), mode='nearest')  # nearest | linear | bilinear | bicubic | trilinear

        x = self.convolutions(x)
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x


class LeNet_BN_DO(nn.Module):
    def __init__(self, input_channels=3, output_classes=10, do=0):
        super().__init__()
        self.convolutions = nn.Sequential(
            nn.Conv2d(input_channels, 6, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(6),
            nn.ReLU(),
            nn.Dropout2d(do),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(6, 16, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.Dropout2d(do),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(16, 120, kernel_size=(5, 5), bias=False),
            nn.BatchNorm2d(120),
            nn.Dropout2d(do),
            nn.ReLU(),
        )
        self.linear = nn.Sequential(
            nn.Linear(120, 84, bias=False),
            nn.BatchNorm1d(84),
            nn.Dropout(do),
            nn.ReLU(),
            nn.Linear(84, output_classes),
        )

    def forward(self, x: torch.Tensor):
        x = F.interpolate(x, size=(32, 32), mode='nearest')  # nearest | linear | bilinear | bicubic | trilinear

        x = self.convolutions(x)
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x



class SqueezeExitation(nn.Module):
    def __init__(self, n_channels, expansion=5):
        super().__init__()
        self.se = nn.Sequential(
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Conv2d(n_channels, expansion * n_channels, (1, 1)),
            nn.ReLU(),
            nn.Conv2d(expansion * n_channels, n_channels, (1, 1)),
            nn.Sigmoid()
        )

    def forward(self, x):
        x = x * self.se(x)
        return x


class LeNet_BN_DO_SE(nn.Module):
    def __init__(self, input_channels=3, output_classes=10, do=0):
        super().__init__()
        self.convolutions = nn.Sequential(
            nn.Conv2d(input_channels, 6, kernel_size=(5, 5), bias=False),
            SqueezeExitation(6),
            nn.BatchNorm2d(6),
            nn.ReLU(),
            nn.Dropout2d(do),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(6, 16, kernel_size=(5, 5), bias=False),
            SqueezeExitation(16),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.Dropout2d(do),
            nn.AvgPool2d(kernel_size=(2, 2), stride=(2, 2)),
            nn.Conv2d(16, 120, kernel_size=(5, 5), bias=False),
            SqueezeExitation(120),
            nn.BatchNorm2d(120),
            nn.ReLU(),
            nn.Dropout2d(do),
        )
        self.linear = nn.Sequential(
            nn.Linear(120, 84, bias=False),
            nn.BatchNorm1d(84),
            nn.Dropout2d(do),
            nn.ReLU(),
            nn.Linear(84, output_classes),
        )

    def forward(self, x: torch.Tensor):
        x = F.interpolate(x, size=(32, 32), mode='nearest')  # nearest | linear | bilinear | bicubic | trilinear

        x = self.convolutions(x)
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x



class IBB(nn.Module):
    def __init__(self, in_channels, expansion=5, do=0, stochastic_do=0):
        super().__init__()
        self.expansion = expansion
        self.stochastic_do = stochastic_do
        self.block = nn.Sequential(
            nn.Conv2d(in_channels, in_channels * self.expansion, (1, 1), bias=False),
            nn.BatchNorm2d(in_channels * self.expansion),
            nn.ReLU(),
            nn.Dropout2d(p=do),
            nn.Conv2d(in_channels * self.expansion, in_channels * self.expansion, (3, 3), padding=(1, 1), bias=False),
            nn.BatchNorm2d(in_channels * self.expansion),
            nn.ReLU(),
            nn.Dropout2d(p=do),
            nn.Conv2d(in_channels * self.expansion, in_channels, (1, 1), bias=False),
            nn.BatchNorm2d(in_channels),
            nn.ReLU(),
            nn.Dropout2d(p=do),
            SqueezeExitation(in_channels),
        )

    def forward(self, x):
        if not self.training or random() > self.stochastic_do:
            x_ = self.block(x)
            x = x + x_
        return x

    
class MNet(nn.Module):
    def __init__(self, in_channels=3, out_channels=10, base_width=30, do=0, stochastic_do=0, expansion=5):
        super().__init__()
        self.out_channels = out_channels
        self.base_width = base_width
        self.do = do
        self.prepare = nn.Sequential(
            nn.Conv2d(in_channels, base_width, (5, 5), bias=False)
        )
        self.main_blocks = nn.Sequential(
            *[IBB(base_width, stochastic_do=stochastic_do, do=do, expansion=expansion) for _ in range(10)],
        )
        self.classifier = nn.Sequential(
            nn.AdaptiveAvgPool2d(1),
            nn.BatchNorm2d(base_width),
            nn.Dropout2d(p=do),
            nn.ReLU(),
            nn.Conv2d(base_width, base_width, (1, 1), bias=False),
            nn.BatchNorm2d(base_width),
            nn.Dropout2d(p=do),
            nn.ReLU(),
            nn.Conv2d(base_width, base_width, (1, 1), bias=False),
            nn.BatchNorm2d(base_width),
            nn.Dropout2d(p=do),
            nn.ReLU(),
        )
        self.final_fc = nn.Linear(base_width, out_channels)

    def forward(self, x):
        x = self.prepare(x)
        x = self.main_blocks(x)
        x = self.classifier(x)
        x = x.view(-1, self.base_width)
        x = self.final_fc(x)
        return x


if __name__ == '__main__':
    summary(MNet(in_channels=3, out_channels=10, base_width=30, do=0, stochastic_do=0, expansion=5), (1, 3, 32, 32),
            row_settings=("depth", "var_names"),
            col_names=("input_size", "kernel_size", "output_size", "num_params"))
